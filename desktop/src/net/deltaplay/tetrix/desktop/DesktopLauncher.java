package net.deltaplay.tetrix.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import net.deltaplay.tetrix.Tetrix;

public class DesktopLauncher {
    public static void main(String[] arg) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.width = 640;
        config.height = 720;
        config.resizable = false;
        new LwjglApplication(new Tetrix(), config);
    }
}
