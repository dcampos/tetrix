package net.deltaplay.tetrix.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Vector2;

public class MovementComponent implements Component {
    // public Vector2 velocity = new Vector2();
    public float speed = 0;
    public Vector2 accel = new Vector2();

    public MovementComponent setSpeed(float speed) {
        this.speed = speed;
        return this;
    }

    // public MovementComponent setVelocity(float x, float y) {
    //     velocity.set(x, y);
    //     return this;
    // }
}
