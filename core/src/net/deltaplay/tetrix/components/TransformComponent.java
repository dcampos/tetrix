package net.deltaplay.tetrix.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Vector3;

public class TransformComponent implements Component {
    public Vector3 pos = new Vector3();
    public Vector3 startpos = new Vector3();
    public float rotation = 0.0f;
    public int rowsToShift = 0;

    public TransformComponent setPosition(float x, float y, float z) {
        this.pos.set(x, y, 0);
        this.startpos.set(pos);
        return this;
    }
}
