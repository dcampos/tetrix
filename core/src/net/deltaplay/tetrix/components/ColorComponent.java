package net.deltaplay.tetrix.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.Color;

/**
 * Created by dpc on 03/05/16.
 */
public class ColorComponent implements Component {
    public Color color;

    public ColorComponent setColor(Color color) {
        this.color = color;
        return this;
    }
}
