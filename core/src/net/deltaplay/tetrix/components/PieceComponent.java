package net.deltaplay.tetrix.components;

import com.badlogic.ashley.core.Component;

public class PieceComponent implements Component {
    public static final float DROP_TIME = 1.0f / 20.0f;
    public static final float SHIFT_TIME = 1.0f / 10.0f;

    public int[][][] pieceMap;
    public float movingTime;

    public float lockTime = 0.1f;

    public boolean rotatePressed = false;
    public boolean dropPressed = false;
    public boolean hardDropPressed = false;
    public boolean leftPressed = false;
    public boolean rightPressed = false;

    public int currentMap = 0;

    public PieceComponent setPieceMap(int[][][] pieceMap) {
        this.pieceMap = pieceMap;
        return this;
    }
}
