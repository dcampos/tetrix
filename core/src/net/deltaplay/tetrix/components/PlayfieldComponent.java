package net.deltaplay.tetrix.components;

import com.badlogic.ashley.core.Component;

public class PlayfieldComponent implements Component{
    public static final int MATRIX_WIDTH = 10;
    public static final int MATRIX_HEIGHT = 18;
}
