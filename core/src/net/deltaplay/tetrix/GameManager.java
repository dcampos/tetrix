package net.deltaplay.tetrix;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import net.deltaplay.tetrix.components.PlayfieldComponent;
import net.deltaplay.tetrix.factories.PieceFactory.PieceType;
import react.IntValue;
import react.Value;

import java.util.Arrays;

public class GameManager {
    public static final float START_SPEED = 48f / 60f;

    public static final float ANIMATION_TIME = 1f;

    public static GameManager instance = new GameManager();

    public enum State {
        RUNNING, PAUSED, SCORING, ANIMATING, CLEANUP, FINISHED
    }

    public Value<State> state = Value.create(State.RUNNING);

    public IntValue score = new IntValue(0);
    public IntValue lines = new IntValue(0);
    public IntValue totalLines = new IntValue(0);
    public IntValue level = new IntValue(0);

    public float speed = START_SPEED;

    public float animationTime = 0f;

    public PieceType nextPiece;

    public final byte[][] map = new byte[PlayfieldComponent.MATRIX_HEIGHT][PlayfieldComponent.MATRIX_WIDTH];

    GameListener listener;

    public void reset() {
        score.update(0);
        lines.update(0);
        totalLines.update(0);
        level.update(0);
        speed = START_SPEED;
        state.update(State.RUNNING);
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[i].length; j++) {
                map[i][j] = 0;
            }
        }
        generateNextPiece();
    }

    public void generateNextPiece() {
        int rand = MathUtils.random(PieceType.values().length - 1);
        nextPiece = PieceType.values()[rand];
    }

    public void addLines(int size) {
        lines.increment(size);
        totalLines.increment(size);
    }

    public void increaseLevel() {
        this.level.increment(1);

        int levelNumber = level.get();
        if (levelNumber <= 8) {
            speed = (48f - (levelNumber * 5f)) / 60f;
        } else if (levelNumber == 9) {
            speed = 6f / 60f;
        } else if (levelNumber >= 10 && levelNumber <= 12) {
            speed = 5f / 60f;
        } else if (levelNumber >= 13 && levelNumber <= 15) {
            speed = 4f / 60f;
        } else if (levelNumber >= 16 && levelNumber <= 18) {
            speed = 3f / 60f;
        } else if (levelNumber >= 19 && levelNumber <= 28) {
            speed = 2f / 60f;
        } else if (levelNumber >= 29) {
            speed = 1f / 60f;
        }

        lines.decrementClamp(10, 0);

        Gdx.app.log(getClass().getName(), "Level increased to " + level.get() + ", speed to " + speed + ".");

    }

    public void applyScore(int lines) {
        int levelNumber = level.get();
        switch (lines) {
            case 1:
                score.increment(40 * (levelNumber + 1));
                break;

            case 2:
                score.increment(100 * (levelNumber + 1));
                break;

            case 3:
                score.increment(300 * (levelNumber + 1));
                break;

            case 4:
                score.increment(1200 * (levelNumber + 1));
                break;
        }
    }

    public void setListener(GameListener listener) {
        this.listener = listener;
    }

    public void printMap() {
        for (int i = 0; i < map.length; i++) {
            System.out.println(Arrays.toString(map[i]));
        }
    }

    public interface GameListener {
        void stateChanged(State state);
    }
}
