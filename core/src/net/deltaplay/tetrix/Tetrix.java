package net.deltaplay.tetrix;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.FPSLogger;
import net.deltaplay.tetrix.screens.GameScreen;

public class Tetrix extends Game {
    FPSLogger fps;

    @Override
    public void create() {
        fps = new FPSLogger();
        setScreen(new GameScreen(this));
    }

    @Override
    public void render() {
        super.render();
        fps.log();
    }
}
