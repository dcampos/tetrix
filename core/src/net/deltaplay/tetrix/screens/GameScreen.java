package net.deltaplay.tetrix.screens;

import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import net.deltaplay.tetrix.GameManager;
import net.deltaplay.tetrix.GameManager.GameListener;
import net.deltaplay.tetrix.GameManager.State;
import net.deltaplay.tetrix.ScoringService;
import net.deltaplay.tetrix.ScoringService.HighScore;
import net.deltaplay.tetrix.Tetrix;
import net.deltaplay.tetrix.components.PlayfieldComponent;
import net.deltaplay.tetrix.factories.PieceFactory;
import net.deltaplay.tetrix.systems.*;
import react.Value;

import java.util.Comparator;

public class GameScreen extends ScreenAdapter implements GameListener {
    Tetrix game;
    PooledEngine engine;
    FitViewport viewport;

    private Stage stage;
    private Label scoreLabel;
    private Label levelLabel;
    private Vector3 tmp;
    private Label linesLabel;
    private Label pausedLabel;
    private Label gameOverLabel;
    private Table scoresTable;

    private Array<HighScore> highScores;

    public GameScreen(Tetrix game) {
        this.game = game;
        this.engine = new PooledEngine();

        float viewportWidth = PlayfieldComponent.MATRIX_WIDTH + 6;
        float viewportHeight = PlayfieldComponent.MATRIX_HEIGHT;
        System.out.println("vw=" + viewportWidth + ", vh=" + viewportHeight);
        this.viewport = new FitViewport(viewportWidth, viewportHeight);

        Gdx.input.setInputProcessor(new InputAdapter() {
            @Override
            public boolean keyDown(int keycode) {
                if (keycode == Keys.R) {
                    startGame();
                } else if (keycode == Keys.P) {
                    if (GameManager.instance.state.get() == State.PAUSED) {
                        resumeGame();
                        GameManager.instance.state.update(State.RUNNING);
                    } else if (GameManager.instance.state.get() == State.RUNNING){
                        pauseGame();
                        GameManager.instance.state.update(State.PAUSED);
                    }
                }

                return true;
            }
        });

        stage = new Stage(new ScreenViewport());

        //createTestBlocks();

        HighScore[] hs = {};

        engine.addSystem(new PieceSystem());
        engine.addSystem(new TransformSystem());
        engine.addSystem(new ScoringSystem());
        engine.addSystem(new AnimationSystem());
        engine.addSystem(new CleanupSystem());
        engine.addSystem(new RenderingSystem(viewport));

        //ScoringService.writeHighScores(hs);
        highScores = new Array<HighScore>();
        highScores.addAll(ScoringService.readHighScores());
    }

    private void pauseGame() {
        engine.getSystem(PieceSystem.class).setProcessing(false);
        engine.getSystem(TransformSystem.class).setProcessing(false);
        engine.getSystem(ScoringSystem.class).setProcessing(false);
        if (!pausedLabel.isVisible()) pausedLabel.setVisible(true);
    }

    private void resumeGame() {
        engine.getSystem(PieceSystem.class).setProcessing(true);
        engine.getSystem(TransformSystem.class).setProcessing(true);
        engine.getSystem(ScoringSystem.class).setProcessing(true);
        if (pausedLabel.isVisible()) pausedLabel.setVisible(false);
    }

    private void startGame() {
        engine.removeAllEntities();
        GameManager.instance.reset();

        PieceFactory.createPiece(engine,
                GameManager.instance.nextPiece);
        GameManager.instance.generateNextPiece();

        resumeGame();

        gameOverLabel.setVisible(false);
        scoresTable.setVisible(false);
    }

    @Override
    public void show() {
        super.show();
        Skin skin = new Skin(Gdx.files.internal("uiskin.json"));
        scoreLabel = new Label("0000", skin);

        GameManager.instance.score.connect(new Value.Listener<Integer>() {
            @Override
            public void onChange(Integer value, Integer oldValue) {
                scoreLabel.setText("SCORE: " + padInteger("000000", value));
            }
        });

        String scoreString = String.valueOf((int)GameManager.instance.score.get());
        scoreLabel.setText("SCORE: " + ("000000" + scoreString).substring(
                scoreString.length()));

        levelLabel = new Label("0000", skin);
        linesLabel = new Label("0000", skin);
        pausedLabel = new Label("PAUSED", skin);
        gameOverLabel = new Label("GAME OVER", skin);
        scoresTable = new Table(skin);

        pausedLabel.addAction(Actions.forever(Actions.sequence(Actions.fadeOut(0.5f),
                Actions.fadeIn(0.5f))));

       stage.addActor(scoresTable);

        stage.addActor(scoreLabel);
        stage.addActor(levelLabel);
        stage.addActor(linesLabel);
        stage.addActor(pausedLabel);
        stage.addActor(gameOverLabel);
        pausedLabel.setVisible(false);
        gameOverLabel.setVisible(false);

        GameManager.instance.setListener(this);

        GameManager.instance.state.connect(new Value.Listener<State>() {
            @Override
            public void onChange(State value, State oldValue) {
                if (value.equals(State.FINISHED)) {
                    gameOverLabel.setVisible(true);
                    scoresTable.setVisible(true);
                    highScores.add(new HighScore(GameManager.instance.totalLines.get(),
                        GameManager.instance.level.get(), GameManager.instance.score.get()));
                    highScores.sort(new Comparator<HighScore>() {
                        @Override
                        public int compare(HighScore o1, HighScore o2) {
                            return (int) Math.signum(o2.score - o1.score);
                        }
                    });
                    if (highScores.size > 10)
                        highScores.removeIndex(highScores.size - 1);
                    ScoringService.writeHighScores(highScores);
                    updateScoresTable();
                }
            }
        });
        startGame();
   }

    private String padInteger(String s, Integer value) {
        String vString = String.valueOf(value);
        return (("000000" + vString).substring(
                vString.length()));
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height, true);
        //stage.getViewport().update(width, height, true);

        Vector3 pos = new Vector3(PlayfieldComponent.MATRIX_WIDTH + 1, 2, 0);
        convertToScreen(pos);
        scoreLabel.setPosition(pos.x, pos.y);

        pos.set(PlayfieldComponent.MATRIX_WIDTH + 1, 3, 0);
        convertToScreen(pos);
        levelLabel.setPosition(pos.x, pos.y);

        pos.set(PlayfieldComponent.MATRIX_WIDTH + 1, 1, 0);
        convertToScreen(pos);
        linesLabel.setPosition(pos.x, pos.y);

        pos.set(PlayfieldComponent.MATRIX_WIDTH / 2, PlayfieldComponent.MATRIX_HEIGHT / 2, 0);
        convertToScreen(pos);
        pausedLabel.setPosition(pos.x - pausedLabel.getWidth() / 2, pos.y);
        gameOverLabel.setPosition(pos.x -gameOverLabel.getWidth() / 2, pos.y);

        pos.set(PlayfieldComponent.MATRIX_WIDTH / 2, PlayfieldComponent.MATRIX_HEIGHT / 2, 0);
        convertToScreen(pos);
        scoresTable.pack();
        scoresTable.setPosition(pos.x - scoresTable.getWidth() / 2,
                pos.y - scoresTable.getHeight());
    }

    private Vector3 convertToScreen(Vector3 pos) {
        viewport.project(pos);
        System.out.println(pos);
        //stage.getViewport().unproject(pos);
        System.out.println(pos);
        return pos;
    }

    private void updateScoresTable() {
        Skin skin = new Skin(Gdx.files.internal("uiskin.json"));

        scoresTable.clear();

        scoresTable.add("SCORE").pad(10);
        scoresTable.add("LEVEL").pad(10);
        scoresTable.add("LINES").pad(10);

        for (HighScore highScore : highScores) {
            scoresTable.row().expand();
            scoresTable.add(String.valueOf(highScore.score));
            scoresTable.add(String.valueOf(highScore.level));
            scoresTable.add(String.valueOf(highScore.lines));
        }

        Vector3 pos = new Vector3();

        pos.set(PlayfieldComponent.MATRIX_WIDTH / 2, PlayfieldComponent.MATRIX_HEIGHT / 2, 0);
        convertToScreen(pos);
        scoresTable.pack();
        scoresTable.setPosition(pos.x - scoresTable.getWidth() / 2,
                pos.y - scoresTable.getHeight());
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        engine.update(delta);


        stage.act();


        String levelString = String.valueOf(GameManager.instance.level.get());
        levelLabel.setText("LEVEL: " + ("000000" + levelString).substring(
                levelString.length()));

        String linesString = String.valueOf(GameManager.instance.totalLines.get());
        linesLabel.setText("LINES: " + ("000000" + linesString).substring(
                linesString.length()));
        stage.draw();
    }

    public void stateChanged(State state) {
        if (state.equals(State.FINISHED)) {
            gameOverLabel.setVisible(true);
            scoresTable.setVisible(true);
            highScores.add(new HighScore(GameManager.instance.score.get(),
                GameManager.instance.level.get(), GameManager.instance.totalLines.get()));
        }
    }
}
