package net.deltaplay.tetrix.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import net.deltaplay.tetrix.GameManager;
import net.deltaplay.tetrix.GameManager.State;
import net.deltaplay.tetrix.components.BlockComponent;
import net.deltaplay.tetrix.components.TransformComponent;
import net.deltaplay.tetrix.factories.PieceFactory;

public class ScoringSystem extends IteratingSystem {

    private static ComponentMapper<TransformComponent> tm = ComponentMapper.getFor(TransformComponent.class);
    private static ComponentMapper<BlockComponent> bm = ComponentMapper.getFor(BlockComponent.class);

    private Array<Integer> rows;

    public ScoringSystem() {
        super(Family.all(BlockComponent.class).get());
        rows = new Array<Integer>();
    }

    @Override
    public void update(float deltaTime) {
        if (GameManager.instance.state.get() != State.SCORING) {
            return;
        }

        GameManager.instance.printMap();

        if (rows.size == 0) {

            for (int i = 0; i < GameManager.instance.map.length; i++) {
                boolean full = true;

                for (int j = 0; j < GameManager.instance.map[i].length && full; j++) {
                    if (GameManager.instance.map[i][j] == 0)
                        full = false;
                }

                if (full) rows.add(i);
            }

            if (rows.size > 0) {
                GameManager.instance.applyScore(rows.size);
                GameManager.instance.addLines(rows.size);

                if (GameManager.instance.lines.get() >= 10) {
                    GameManager.instance.increaseLevel();
                }
                GameManager.instance.state.update(State.ANIMATING);
            } else {
                Entity piece = PieceFactory.createPiece((PooledEngine) getEngine(),
                        GameManager.instance.nextPiece);
                GameManager.instance.generateNextPiece();

                GameManager.instance.state.update(State.RUNNING);
            }
        }

        super.update(deltaTime);
        rows.clear();
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        TransformComponent tc = tm.get(entity);
        BlockComponent bc = bm.get(entity);

        int y = MathUtils.round(tc.pos.y);

        // Remoção dos blocos deve acontecer apenas após animação
        //GameManager.instance.map[MathUtils.round(tc.pos.y)][MathUtils.round(tc.pos.x)] = 0;

        if (rows.contains(y, true)) {
            bc.toRemove = true;
        } else {
            int shift = 0;
            for (int row : rows) {
                if (row > y) {
                    shift++;
                }
            }

            tc.rowsToShift = shift;

        }
    }
}
