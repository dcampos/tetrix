package net.deltaplay.tetrix.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IntervalIteratingSystem;
import net.deltaplay.tetrix.components.MovementComponent;

/**
 * Created by dpc on 08/05/16.
 */
public class MovementSystem extends IntervalIteratingSystem {

    public MovementSystem() {
        super(Family.all(MovementComponent.class).get(), 0.5f);
    }

    @Override
    protected void processEntity(Entity entity) {

    }
}
