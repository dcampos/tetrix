package net.deltaplay.tetrix.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.math.MathUtils;
import net.deltaplay.tetrix.GameManager;
import net.deltaplay.tetrix.GameManager.State;
import net.deltaplay.tetrix.components.*;
import net.deltaplay.tetrix.factories.BlockFactory;

public class TransformSystem extends IteratingSystem {

    private static ComponentMapper<TransformComponent> tm = ComponentMapper.getFor(TransformComponent.class);
    private static ComponentMapper<MovementComponent> mm = ComponentMapper.getFor(MovementComponent.class);
    private static ComponentMapper<PieceComponent> pm = ComponentMapper.getFor(PieceComponent.class);

    public TransformSystem() {
        super(Family.one(TransformComponent.class).get());
    }

    @Override
    public void update(float deltaTime) {
        for (int i = 0; i < GameManager.instance.map.length; i++) {
            //Arrays.fill(GameState.map[i], 0);
        }

        super.update(deltaTime);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        if (GameManager.instance.state.get() != State.RUNNING) return;

        if (entity.getComponent(PieceComponent.class) != null) {
            processPiece(entity);
        } else if (entity.getComponent(BlockComponent.class) != null) {
            TransformComponent tc = tm.get(entity);
            int x = MathUtils.round(tc.pos.x);
            int y = MathUtils.round(tc.pos.y);
            //GameState.map[y][x] = 1;
        }

    }

    private void processPiece(Entity entity) {
        TransformComponent tc = tm.get(entity);
        MovementComponent mc = mm.get(entity);
        PieceComponent pc = pm.get(entity);

        if (!fitsInto(entity, pc.currentMap, tc.pos.x, tc.pos.y)) {
            System.out.println("GAME OVER");
            System.out.println("SCORE=" + GameManager.instance.score);
            GameManager.instance.state.update(State.FINISHED);
            return;
        }

        if (pc.lockTime == 0) {
            if (pc.rotatePressed) {
                int newIndex = pc.currentMap + 1;
                if (newIndex >= pc.pieceMap.length) newIndex = 0;

                if (fitsInto(entity, newIndex, tc.pos.x, tc.pos.y)) {
                    pc.currentMap = newIndex;
                }

                pc.rotatePressed = false;
                pc.lockTime = 1f / 5f;
            } else if (pc.dropPressed) {
                float ny = tc.pos.y + 1;

                if (fitsInto(entity, pc.currentMap, tc.pos.x, ny)) {
                    tc.pos.y = ny;
                }

                pc.dropPressed = false;
                pc.lockTime = PieceComponent.DROP_TIME;

            } else if (pc.hardDropPressed) {

                float ny = tc.pos.y;

                while (fitsInto(entity, pc.currentMap, tc.pos.x, ny)){
                    tc.pos.y = ny;
                    ny++;
                }

                pc.hardDropPressed = false;
                pc.lockTime = PieceComponent.SHIFT_TIME;
            } else if (pc.leftPressed || pc.rightPressed) {
               float nx = pc.leftPressed ? tc.pos.x - 1 : tc.pos.x + 1;

                if (fitsInto(entity, pc.currentMap, nx, tc.pos.y)) {
                    tc.pos.x = nx;
                }

                if (pc.leftPressed)
                    pc.leftPressed = false;
                else
                    pc.rightPressed = false;

                pc.lockTime = PieceComponent.SHIFT_TIME;
            }
        }

        // TODO: fazer o movimento ser unitário
        if (pc.movingTime > GameManager.instance.speed) {
            if (fitsInto(entity, pc.currentMap, tc.pos.x, tc.pos.y + 1)) {
                tc.pos.y += 1;
                pc.movingTime -= GameManager.instance.speed;
            } else {
                BlockFactory.createBlocksFromPiece(entity, (PooledEngine) getEngine());
                getEngine().removeEntity(entity);

                //PieceComponent npc = pm.get(piece);
                //npc.lockTime = currentLockTime;

                GameManager.instance.state.update(State.SCORING);
           }

            if (tc.pos.y < -2) {
                getEngine().removeEntity(entity);
            }
        }
    }

    public boolean fitsInto(Entity piece, int mapIndex, float x, float y) {
        PieceComponent pc = pm.get(piece);

        int[][] map = pc.pieceMap[mapIndex];

        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[i].length; j++) {
                if (map[i][j] == 1) {
                    int nx = MathUtils.round(x) + j;
                    int ny = MathUtils.round(y) + i;

                    if (nx < 0 || nx >= PlayfieldComponent.MATRIX_WIDTH) {
                        return false;
                    }

                    if (ny < 0 || ny >= PlayfieldComponent.MATRIX_HEIGHT) {
                        return false;
                    }

                    if (GameManager.instance.map[ny][nx] == 1) {
                        return false;
                    }
                }
            }
        }

        return true;
    }
}
