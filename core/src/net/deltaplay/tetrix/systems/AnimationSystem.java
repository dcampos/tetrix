package net.deltaplay.tetrix.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.graphics.Color;
import net.deltaplay.tetrix.GameManager;
import net.deltaplay.tetrix.GameManager.State;
import net.deltaplay.tetrix.components.BlockComponent;
import net.deltaplay.tetrix.components.ColorComponent;
import net.deltaplay.tetrix.components.TransformComponent;

public class AnimationSystem extends IteratingSystem {
    private static ComponentMapper<BlockComponent> bm = ComponentMapper.getFor(BlockComponent.class);
    private static ComponentMapper<ColorComponent> cm = ComponentMapper.getFor(ColorComponent.class);
    private static ComponentMapper<TransformComponent> tm = ComponentMapper.getFor(TransformComponent.class);

    private final Color color = Color.WHITE.cpy();

    public AnimationSystem() {
        super(Family.all(BlockComponent.class).get());
    }

    @Override
    public void update(float deltaTime) {
        if (GameManager.instance.state.get() != State.ANIMATING)
            return;

        GameManager.instance.animationTime += deltaTime;

        if (GameManager.instance.animationTime > (GameManager.ANIMATION_TIME * GameManager.instance.speed)) {
            GameManager.instance.animationTime = 0;
            GameManager.instance.state.update(State.CLEANUP);
        } else {
            color.a = (GameManager.instance.animationTime * 4 % 1);
            super.update(deltaTime);
        }
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {

        BlockComponent bc = bm.get(entity);
        ColorComponent cc = cm.get(entity);
        TransformComponent tc = tm.get(entity);

        if (bc.toRemove)
            cc.color = color;
    }
}
