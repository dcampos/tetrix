package net.deltaplay.tetrix.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.math.MathUtils;
import net.deltaplay.tetrix.GameManager;
import net.deltaplay.tetrix.GameManager.State;
import net.deltaplay.tetrix.components.BlockComponent;
import net.deltaplay.tetrix.components.TransformComponent;
import net.deltaplay.tetrix.factories.PieceFactory;

import java.util.Arrays;

public class CleanupSystem extends IteratingSystem {
    private static ComponentMapper<TransformComponent> tm = ComponentMapper.getFor(TransformComponent.class);
    private static ComponentMapper<BlockComponent> bm = ComponentMapper.getFor(BlockComponent.class);

    public CleanupSystem() {
        super(Family.all(BlockComponent.class, TransformComponent.class).get());
    }

    @Override
    public void update(float deltaTime) {
        if (GameManager.instance.state.get() != State.CLEANUP)
            return;

        for (int i = 0; i < GameManager.instance.map.length; i++) {
            Arrays.fill(GameManager.instance.map[i], (byte) 0);
        }

        super.update(deltaTime);

        PieceFactory.createPiece((PooledEngine) getEngine(),
                GameManager.instance.nextPiece);
        GameManager.instance.generateNextPiece();
        GameManager.instance.state.update(State.RUNNING);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        BlockComponent bc = bm.get(entity);
        TransformComponent tc = tm.get(entity);

        if (bc.toRemove) {
            // Limpa os blocos removidos
            getEngine().removeEntity(entity);
        } else {
            // Move os blocos de cima
            tc.pos.y += tc.rowsToShift;
            tc.rowsToShift = 0;
            GameManager.instance.map[MathUtils.round(tc.pos.y)][MathUtils.round(tc.pos.x)] = 1;
        }

    }
}
