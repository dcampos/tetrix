package net.deltaplay.tetrix.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import net.deltaplay.tetrix.components.PlayfieldComponent;

public class PlayfieldSystem extends IteratingSystem {

    public PlayfieldSystem() {
        super(Family.one(PlayfieldComponent.class).get());
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {

    }
}
