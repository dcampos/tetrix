package net.deltaplay.tetrix.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IntervalIteratingSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import net.deltaplay.tetrix.components.MovementComponent;
import net.deltaplay.tetrix.components.PieceComponent;
import net.deltaplay.tetrix.components.TransformComponent;

public class PieceSystem extends IntervalIteratingSystem {
    public static final float INTERVAL = 1f / 30f;

    private ComponentMapper<TransformComponent> tm = ComponentMapper.getFor(TransformComponent.class);
    private ComponentMapper<MovementComponent> mm = ComponentMapper.getFor(MovementComponent.class);
    private ComponentMapper<PieceComponent> pm = ComponentMapper.getFor(PieceComponent.class);

    public PieceSystem() {
        super(Family.all(MovementComponent.class, PieceComponent.class, TransformComponent.class).get(), INTERVAL);
    }

    @Override
    protected void processEntity(Entity entity) {
        TransformComponent tc = tm.get(entity);
        MovementComponent mc = mm.get(entity);
        PieceComponent pc = pm.get(entity);

        pc.movingTime += getInterval();

        boolean hasMoved = tc.pos.y > tc.startpos.y;

        if (pc.lockTime > 0) {
            pc.lockTime -= getInterval();
            if (pc.lockTime < 0) pc.lockTime = 0;
        } else {

            if (Gdx.input.isKeyPressed(Keys.UP)) {
                pc.rotatePressed = true;
            } else if (Gdx.input.isKeyPressed(Keys.DOWN)) {
                pc.dropPressed = true;
            } else if (Gdx.input.isKeyPressed(Keys.SPACE) && hasMoved) {
                pc.hardDropPressed = true;
            } else if (Gdx.input.isKeyPressed(Keys.LEFT)) {
                pc.leftPressed = true;
            } else if (Gdx.input.isKeyPressed(Keys.RIGHT)) {
                pc.rightPressed = true;
            }
        }

   }
}
