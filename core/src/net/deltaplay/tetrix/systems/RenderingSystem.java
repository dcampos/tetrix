package net.deltaplay.tetrix.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.SortedIteratingSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.Viewport;
import net.deltaplay.tetrix.GameManager;
import net.deltaplay.tetrix.GameManager.State;
import net.deltaplay.tetrix.components.ColorComponent;
import net.deltaplay.tetrix.components.PieceComponent;
import net.deltaplay.tetrix.components.PlayfieldComponent;
import net.deltaplay.tetrix.components.TransformComponent;
import net.deltaplay.tetrix.factories.PieceFactory.PieceType;

import java.util.Comparator;

public class RenderingSystem extends SortedIteratingSystem {
    public static final float BLOCK_SIZE = 1;
    public static final float BORDER_SIZE = 0.2f;
    public static final Color GRAY1 = Color.DARK_GRAY;
    public static final Color GRAY2 = new Color(GRAY1).mul(1.2f, 1.2f, 1.2f, 1);

    private boolean displayBox = false;

    private static ComponentMapper<TransformComponent> tm = ComponentMapper.getFor(TransformComponent.class);
    private static ComponentMapper<ColorComponent> cm = ComponentMapper.getFor(ColorComponent.class);
    private static ComponentMapper<PieceComponent> pm = ComponentMapper.getFor(PieceComponent.class);

    private Array<Entity> renderQueue;

    private ShapeRenderer shapeRenderer;
    private SpriteBatch batch;
    private Viewport viewport;

    private TextureRegion blockRegion;

    public RenderingSystem(Viewport viewport) {
        super(Family.all(ColorComponent.class, TransformComponent.class).get(),
            new BlockComparator());

        this.renderQueue = new Array<Entity>();

        this.batch = new SpriteBatch();
        this.shapeRenderer = new ShapeRenderer();
        this.viewport = viewport;

        blockRegion = new TextureRegion(new Texture(Gdx.files.internal("data/block.png")));
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        renderQueue.add(entity);
    }


    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);

        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        Gdx.gl.glLineWidth(2.0f);


        shapeRenderer.setProjectionMatrix(viewport.getCamera().combined);

        shapeRenderer.setAutoShapeType(true);
        shapeRenderer.begin(ShapeType.Filled);

        renderBackground(0, 0, PlayfieldComponent.MATRIX_WIDTH, PlayfieldComponent.MATRIX_HEIGHT);

        for (Entity entity : renderQueue) {
            TransformComponent tc = tm.get(entity);
            ColorComponent cc = cm.get(entity);
            PieceComponent pc = pm.get(entity);
            shapeRenderer.setColor(Color.BLACK);
            if (pc != null) {

                int[][] map = pc.pieceMap[pc.currentMap];

                for (int i = 0; i < map.length; i++) {
                    for (int j = 0; j < map[0].length; j++) {
                        if (map[i][j] == 0)
                            continue;

                        float yPos = tc.pos.y + i * BLOCK_SIZE;
                        float xPos = tc.pos.x + j * BLOCK_SIZE;

                        renderBlock(xPos, PlayfieldComponent.MATRIX_HEIGHT - 1 - yPos, cc.color);
                    }
                }
            } else {
                renderBlock(tc.pos.x, PlayfieldComponent.MATRIX_HEIGHT - 1 - tc.pos.y, cc.color);
            }
        }

        if (GameManager.instance.state.get() == State.FINISHED) {
            shapeRenderer.set(ShapeType.Filled);
            shapeRenderer.setColor(new Color(0X000000AA));
            shapeRenderer.rect(0, 0, PlayfieldComponent.MATRIX_WIDTH,
                    PlayfieldComponent.MATRIX_HEIGHT);
        }
        renderNextPiece();
        shapeRenderer.end();

        Gdx.gl.glDisable(GL20.GL_BLEND);

        renderQueue.clear();
    }

    private void renderNextPiece() {
        float offsetX = PlayfieldComponent.MATRIX_WIDTH + 1;
        float offsetY = PlayfieldComponent.MATRIX_HEIGHT - 5;
        renderBackground(offsetX, offsetY, 4, 4);

        PieceType pt = GameManager.instance.nextPiece;

        int[][] map = pt.map[0];

        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[0].length; j++) {
                if (map[i][j] == 0)
                    continue;

                float yPos = 1 + i * BLOCK_SIZE;
                float xPos = offsetX + j * BLOCK_SIZE;

                renderBlock(xPos, PlayfieldComponent.MATRIX_HEIGHT - 1 - yPos, pt.color);
            }
        }

    }

    private void renderBlock(float x, float y, Color color) {
        shapeRenderer.set(ShapeType.Filled);
        shapeRenderer.setColor(color);
        shapeRenderer.rect(x, y, BLOCK_SIZE, BLOCK_SIZE);
        shapeRenderer.setColor(Color.BLACK);
        shapeRenderer.set(ShapeType.Line);
        shapeRenderer.rect(x, y, BLOCK_SIZE, BLOCK_SIZE);
    }

    private void renderBackground(float offsetX, float offsetY, int width, int height) {
        shapeRenderer.set(ShapeType.Filled);
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if ((i + j) % 2 == 0)
                    shapeRenderer.setColor(GRAY1);
                else
                    shapeRenderer.setColor(GRAY2);

                shapeRenderer.rect(i * BLOCK_SIZE + offsetX, j * BLOCK_SIZE + offsetY,
                    BLOCK_SIZE, BLOCK_SIZE);
            }
        }
    }

    private static class BlockComparator implements Comparator<Entity> {
        @Override
        public int compare(Entity e1, Entity e2) {
            int ycmp = (int) Math.signum(tm.get(e1).pos.y - tm.get(e2).pos.y);
            // return ycmp == 0 ? (int) Math.signum(tm.get(e1).pos.x - tm.get(e2).pos.x)
            //        : ycmp;
            return ycmp;
        }
    }
}
