package net.deltaplay.tetrix.factories;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.gdx.graphics.Color;
import net.deltaplay.tetrix.components.ColorComponent;
import net.deltaplay.tetrix.components.MovementComponent;
import net.deltaplay.tetrix.components.PieceComponent;
import net.deltaplay.tetrix.components.TransformComponent;

/**
 * Created by dpc on 06/05/16.
 */
public class PieceFactory {

    public static Entity createPiece(PooledEngine engine, PieceType type) {
        Entity piece = engine.createEntity();

        piece.add(new PieceComponent().setPieceMap(type.map));
        piece.add(new ColorComponent().setColor(type.color));
        int shift = calculateVerticalShift(type.map[0]);
        piece.add(new TransformComponent().setPosition(3, 0 - shift, 0));
        piece.add(new MovementComponent().setSpeed(60f/48f));

        engine.addEntity(piece);

        return piece;
    }

    private static int calculateVerticalShift(int[][] map) {
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[i].length; j++) {
                if (map[i][j] == 1) {
                    return i;
                }
            }
        }

        return 0;
    }

    public enum PieceType {
        I(0x31C7EFFF, new int[][][]
                {{
                        {0, 0, 0, 0},
                        {0, 0, 0, 0},
                        {1, 1, 1, 1},
                        {0, 0, 0, 0}
                }, {
                        {0, 1, 0, 0},
                        {0, 1, 0, 0},
                        {0, 1, 0, 0},
                        {0, 1, 0, 0}
                }}),
        O(0xF7D308FF, new int[][][]
                {{
                        {0, 0, 0, 0},
                        {0, 1, 1, 0},
                        {0, 1, 1, 0},
                        {0, 0, 0, 0}
                },
                }),
        T(0xAD4D9CFF, new int[][][]
                {{
                        {0, 0, 0, 0},
                        {1, 1, 1, 0},
                        {0, 1, 0, 0},
                        {0, 0, 0, 0},
                }, {
                        {0, 1, 0, 0},
                        {1, 1, 0, 0},
                        {0, 1, 0, 0},
                        {0, 0, 0, 0}
                }, {
                        {0, 1, 0, 0},
                        {1, 1, 1, 0},
                        {0, 0, 0, 0},
                        {0, 0, 0, 0}
                }, {
                        {0, 1, 0, 0},
                        {0, 1, 1, 0},
                        {0, 1, 0, 0},
                        {0, 0, 0, 0}
                },}),
        S(0x00FF00FF, new int[][][]
                {{
                        {0, 0, 0, 0},
                        {0, 1, 1, 0},
                        {1, 1, 0, 0},
                        {0, 0, 0, 0}
                }, {
                        {1, 0, 0, 0},
                        {1, 1, 0, 0},
                        {0, 1, 0, 0},
                        {0, 0, 0, 0}
                },}),
        Z(0xFF0000FF, new int[][][]
                {{
                        {0, 0, 0, 0},
                        {1, 1, 0, 0},
                        {0, 1, 1, 0},
                        {0, 0, 0, 0}
                }, {
                        {0, 1, 0, 0},
                        {1, 1, 0, 0},
                        {1, 0, 0, 0},
                        {0, 0, 0, 0}
                }}),
        J(0x0000FFFF, new int[][][]
                {{
                        {0, 0, 0, 0},
                        {1, 1, 1, 0},
                        {0, 0, 1, 0},
                        {0, 0, 0, 0},
                }, {
                        {0, 1, 0, 0},
                        {0, 1, 0, 0},
                        {1, 1, 0, 0},
                        {0, 0, 0, 0},
                }, {
                        {0, 0, 0, 0},
                        {1, 0, 0, 0},
                        {1, 1, 1, 0},
                        {0, 0, 0, 0},
                }, {
                        {0, 1, 1, 0},
                        {0, 1, 0, 0},
                        {0, 1, 0, 0},
                        {0, 0, 0, 0},
                }}),
        L(0xEF7921FF, new int[][][]
                {{
                        {0, 0, 0, 0},
                        {1, 1, 1, 0},
                        {1, 0, 0, 0},
                        {0, 0, 0, 0},
                }, {
                        {1, 1, 0, 0},
                        {0, 1, 0, 0},
                        {0, 1, 0, 0},
                        {0, 0, 0, 0},
                }, {
                        {0, 0, 0, 0},
                        {0, 0, 1, 0},
                        {1, 1, 1, 0},
                        {0, 0, 0, 0},
                }, {
                        {0, 1, 0, 0},
                        {0, 1, 0, 0},
                        {0, 1, 1, 0},
                        {0, 0, 0, 0},
                }});

        public final Color color;
        public final int[][][] map;

        PieceType(int hex, int[][][] map) {
            this.color = new Color(hex);
            this.map = map;
        }
    }
}
