package net.deltaplay.tetrix.factories;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.gdx.math.MathUtils;
import net.deltaplay.tetrix.GameManager;
import net.deltaplay.tetrix.components.BlockComponent;
import net.deltaplay.tetrix.components.ColorComponent;
import net.deltaplay.tetrix.components.PieceComponent;
import net.deltaplay.tetrix.components.TransformComponent;

public class BlockFactory {
    private static ComponentMapper<TransformComponent> tm = ComponentMapper.getFor(TransformComponent.class);
    private static ComponentMapper<PieceComponent> pm = ComponentMapper.getFor(PieceComponent.class);
    private static ComponentMapper<ColorComponent> cm = ComponentMapper.getFor(ColorComponent.class);

    public static void createBlocksFromPiece(Entity piece, PooledEngine engine) {
        TransformComponent tc = tm.get(piece);
        PieceComponent pc = pm.get(piece);
        ColorComponent cc = cm.get(piece);

        int[][] map = pc.pieceMap[pc.currentMap];

        for (int i = map.length - 1; i >= 0; i--) {
            for (int j = 0; j < map[i].length; j++) {
                if (map[i][j] == 1) {
                    int x = MathUtils.round(tc.pos.x + j);
                    int y = MathUtils.round(tc.pos.y + i);

                    Entity block = engine.createEntity();
                    block.add(new TransformComponent().setPosition(x, y, 0));
                    block.add(new ColorComponent().setColor(cc.color));
                    block.add(new BlockComponent());
                    engine.addEntity(block);

                    GameManager.instance.map[y][x] = 1;
                }
            }
        }

    }
}
