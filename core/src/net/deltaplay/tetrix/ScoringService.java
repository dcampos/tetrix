package net.deltaplay.tetrix;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;

public class ScoringService {
    public static final String HIGHSCORES_FILE = ".tetrix";

    public static Array<HighScore> readHighScores() {
        Preferences prefs = Gdx.app.getPreferences(HIGHSCORES_FILE);

        String txt = prefs.getString("highscores", "[]");

        Json parser = new Json();
        Array<HighScore> hs = parser.fromJson(Array.class, HighScore.class, txt);

        return hs;
    }

    public static void writeHighScores(Array<HighScore> highScores) {
        Preferences prefs = Gdx.app.getPreferences(HIGHSCORES_FILE);

        Json parser = new Json();
        String txt = parser.toJson(highScores);

        prefs.putString("highscores", txt);
        prefs.flush();
    }

    public static class HighScore {
        public int lines;
        public int level;
        public int score;

        public HighScore() {

        }

        public HighScore(int lines, int level, int score) {
            this.lines = lines;
            this.level = level;
            this.score = score;
        }

    }
}
